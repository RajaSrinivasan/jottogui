// TODO: Put public facing types in this file.
import 'dictionary.dart' ;
import 'words.dart' ;
class Jotto 
{
  Dictionary _dict = Dictionary();
  String _secretword = '_notset';
  Jotto( [wordlength = 6, wordfile='etc/wordlist.txt'] )
  {
     _dict = new Dictionary(wordlength) ;
     _dict.SetWords(words);
  }
  String Next()
  {
    _secretword = _dict.Select();
    return _secretword ;
  }
  /// Score the guess against the original counting unique characters.
  ///
  /// Strings have to be of the same length. If not returns -1
  /// If the guess matches completely the length of the original string is returned
  /// Each character can only be counted only once
  int Score(String original, String guess) 
  {
      List<String> found = [] ;
      List<String> searched = [] ;
      if (original == guess) 
      {
        return original.length +1 ;
      }
      if (original.length != guess.length) return -1;
      if (_dict.IsWord(guess))
        {

        }
      else
        {
          return -2 ;
        }
      var result = 0 ;
      for (int i=0; i < guess.length; i++)
      {
        //print('${original.substring(i,i+1)} ${guess.substring(i,i+1)}\n');
        // This is somewhat redundant. Even if the candidate contains duplicate characters
        // it will be counted once because of the latter check.
        if (searched.contains(guess.substring(i,i+1)))
        {
          continue ;
        }
        if (found.contains(guess.substring(i,i+1)))
        {
          continue ;
        }
        searched.add(guess.substring(i,i+1)) ;
        if (original.indexOf(guess.substring(i,i+1)) >= 0)
        {
          //print('Found ${guess.substring(i,i+1)}\n') ;
          found.add(guess.substring(i,i+1));
          result ++;
        }
        else
        {
          //print('Did not find ${guess.substring(i,i+1)}\n');
        }
       
      }
      return result;
  }
}