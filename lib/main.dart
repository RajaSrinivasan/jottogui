import 'package:flutter/material.dart';
import 'package:group_button/group_button.dart';
import 'jotto.dart' as jottolib ;

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Jotto',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Jotto'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool _debug = true ;
  int _score=0;
  final gameRules = 'https://en.wikipedia.org/wiki/Jotto' ;
  final myController = TextEditingController();
  var myjottolib = jottolib.Jotto() ;
  String _currentword = '' ; // myjottolib.Next() ;
  List<String> _guesses = [] ;
  List<int>    _scores = [] ;

  bool _solved=false ;
  void _generateNextWord()
  {
      _currentword = myjottolib.Next() ;
      _solved=false ;
  }
  void _checkAnswer()
  {
    if (myController.text == 'Your answer')
    {
      return ;
    }
    _guesses.add(myController.text) ;

    _score = myjottolib.Score(_currentword, myController.text);
    if (_score == 1+_currentword.length)
      {
        _solved=true ;
      }
    _scores.add(_score);
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also a layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.start ,
          children: <Widget>[
            TextButton(
              child: Text( _debug ? '${_currentword}' : '${gameRules}',
                           style: Theme.of(context).textTheme.headline4),
              onPressed: () {
                if (_debug)
                  {
                    _debug = false ;
                  }
                else
                  {
                    _debug = true ;
                  }
                setState(() {

                });
              },

            ),
            Text(
              'Enter your guess',
              style: Theme.of(context).textTheme.headline3,
            ),
            TextFormField(
              style: Theme.of(context).textTheme.headline4,
              textAlign: TextAlign.center,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Your answer',
              ),
              controller: myController,
              keyboardType: TextInputType.text ,
            ),
            Text(
              '$_score',
              style: Theme.of(context).textTheme.headline4,
            ),
            //Icon(Icons.check , color : goodanswer ? Colors.Red , Colors.white) ,
            Icon( _solved ?  Icons.check : Icons.question_mark , color : _solved ? Colors.green : Colors.red , size : 50) ,

            GroupButton(
              isRadio: true,
              //onSelected: (index, isSelected) => print('$index button is selected'),
              onSelected: (val, i, selected) => {
                //debugPrint('Button: $val index: $i $selected');
                if (val == 'Next') {
                  _generateNextWord()
                },
                if (val == 'Check') {
                  _checkAnswer()
                },
                setState(() => {} )
              } ,
              buttons: ["Check","Next"],
            ),
            Expanded(
                child: ListView.builder(
                    itemCount: _guesses.length ,
                    itemBuilder: (BuildContext ctxt, int index)
                  {
                    return  ListTile(
                              leading: Text('${1+index}') ,
                              trailing: Text("${_scores[_scores.length-index-1]}", style: TextStyle(color: Colors.green, fontSize: 15),),
                              title: Text("${_guesses[_guesses.length-index-1]}"));
                  }
            )),
          ],
        ),
      ),
     // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
